//
//  ITunesAPI.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 10.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import Foundation

final class ITunesAPI: APIClient {
	lazy var session: NSURLSession = {
		return NSURLSession(configuration: self.configuration)
	}()
	let configuration: NSURLSessionConfiguration
	
	init(config: NSURLSessionConfiguration) {
		self.configuration = config
	}
	
	convenience init() {
		self.init(config: NSURLSessionConfiguration.defaultSessionConfiguration())
	}
	
	func fetchITunesData(search: ITunes, completion: APIResult<ITunesData> -> Void) {
		let request = search.request
		fetch(request, parse: { json -> ITunesData? in
			if let results = json["results"] as? [JSON],
				firstResult = results[0] as? JSON,
				track = firstResult["trackName"] as? String {
				return ITunesData(track: track, number: 1)
			} else {
				return nil
			}
			
		}, completion: completion)
	}
}

enum ITunes: Endpoint {
	case theKillers
	
	var baseURL: NSURL {
		return NSURL(string: "http://itunes.apple.com/")!
	}
	
	var path: String {
		switch self {
		case .theKillers:
			return "search?term=The%20Killers"
		
		}
	}
	
	var request: NSMutableURLRequest {

		let url = NSURL(string: path, relativeToURL: baseURL)!
		print("url is \(url)")
		return NSMutableURLRequest(URL: url)
	}
}

struct ITunesData {
	let track: String
	let number: Int
}

