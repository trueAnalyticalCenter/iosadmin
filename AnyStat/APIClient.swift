//
//  APIClient.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 10.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import Foundation

typealias JSON = [String: AnyObject]

enum APIResult<T> {
	case success(T)
	case error(ErrorType)
}

protocol Endpoint {
	var baseURL: NSURL { get }
	var path: String { get }
	var request: NSMutableURLRequest { get }
}

protocol APIClient {
	var session: NSURLSession { get }
	var configuration: NSURLSessionConfiguration { get }
	
	func fetch<T>(request: NSURLRequest, parse: JSON -> T?, completion: APIResult<T> -> Void)
}

extension APIClient {
	func fetch<T>(request: NSURLRequest, parse: JSON -> T?, completion: APIResult<T> -> Void) {
		let task = session.dataTaskWithRequest(request) { data, response, error in
			guard let data = data else {
				if let error = error {
					completion(.error(error))
				}
				return
			}
			guard let response = response as? NSHTTPURLResponse else { return }
			
			switch response.statusCode {
			case 200:
				do {
					let jsonOpt = try NSJSONSerialization.JSONObjectWithData(data, options: [])
					guard let json = jsonOpt as? JSON else { return }
					guard let result = parse(json) else { return }
					completion(.success(result))
					print("response is \(response.statusCode)")					
				} catch let error {
					completion(.error(error))
				}
			default:
				print("response is \(response.statusCode)")
				return
			}
		}
		task.resume()
	}
}
