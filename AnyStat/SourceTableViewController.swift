//
//  SourceTableViewController.swift
//  AnyStat
//
//  Created by Alex Kuzovkov on 04.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import UIKit

class SourceTableViewController: UITableViewController {
    var site: Site?
    
    @IBOutlet weak var sourceNameTextField: UITextField!
    
    @IBAction func save(sender: AnyObject) {
        if let site = site {
             site.editName(sourceNameTextField.text!)
        } else {
            Site.addSite(sourceNameTextField.text!)
        }
       
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		print("Site is \(site?.name) now")
        sourceNameTextField.text = site?.name
    }
}
