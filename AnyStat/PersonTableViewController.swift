//
//  PersonTableViewController.swift
//  AnyStat
//
//  Created by Alex Kuzovkov on 04.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import UIKit

class PersonTableViewController: UITableViewController {
    var person:Person?
    
    private var personName : String!
    private var personKeywords : Array<Keyword>!
    
    @IBAction func save(sender: AnyObject) {
        let textField:UITextField = self.view.viewWithTag(100) as! UITextField
        
        if let editedPerson = person {
            editedPerson.editName(textField.text!)
        } else {
            Person.addPerson(textField.text!)
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        personName = person?.name
        personKeywords = person?.getKeywords()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section==0) {
            return 1
        }
    
        if let keywords = personKeywords {
            return keywords.count;
        } else {
            return 0
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.section==0) {
            let cell = tableView.dequeueReusableCellWithIdentifier("editableCell", forIndexPath: indexPath)
            let textField:UITextField = cell.viewWithTag(100) as! UITextField
            textField.text = personName
            return cell;
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("keywordCell", forIndexPath: indexPath)
            cell.textLabel?.text = personKeywords[indexPath.row].name
            return cell;
        }
    }
 
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section==0) {
            return "Name"
        }
        else {
            return "Keywords"
        }
    }

}
