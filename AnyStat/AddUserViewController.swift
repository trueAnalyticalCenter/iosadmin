//
//  AddUserViewController.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 07.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import UIKit

protocol AddUserViewControllerDelegate: class  {
	func addUserViewControllerDidCancel(controller: AddUserViewController)
	func addUserViewController(controller: AddUserViewController, didFinishAdding user: User)
}

class AddUserViewController: UIViewController, UITextFieldDelegate{
	
	@IBOutlet weak var doneButton: UIBarButtonItem!
	
	@IBOutlet weak var nicknameTextField: UITextField!
	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var surnameTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
	
	weak var delegate: AddUserViewControllerDelegate?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		//doneButton.enabled = false
    }

	
	@IBAction func cancel() {
		delegate?.addUserViewControllerDidCancel(self)
	}
	
	@IBAction func done() {
		let name = nameTextField.text!
		let surname = surnameTextField.text!
		let nick = nicknameTextField.text!
		let password = passwordTextField.text!
		let newUser = User(name: name, surname: surname, nickname: nick, password: password)
		delegate?.addUserViewController(self, didFinishAdding: newUser)
		
	}
	
	//method doesn't work(
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		let oldText = textField.text! as NSString
		let newText = oldText.stringByReplacingCharactersInRange(range, withString: string) as NSString
		doneButton.enabled = (newText.length > 0)
		return true
	}
	

	
}
