//
//  PersonPageRank.swift
//  AnyStat
//
//  Created by Vitaly on 03.10.16.
//  Copyright © 2016 True Analytical Center. All rights reserved.
//

import Foundation
import RealmSwift

class PersonPageRank: Object {
    
    // Идентификатор личности, которой соответствует данное ключевое слово.
    dynamic var PersonID: Int = 0
    
    // Идентификатор страницы сайта, на которой найдены упоминания о персонах
    dynamic var PageID: Int = 0
    
    // Количество упоминаний личности на странице 
    dynamic var Rank: Int = 0
    
}
