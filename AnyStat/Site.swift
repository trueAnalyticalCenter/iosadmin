//
//  Site.swift
//  AnyStat
//
//  Created by Vitaly on 03.10.16.
//  Copyright © 2016 True Analytical Center. All rights reserved.
//

import Foundation
import RealmSwift

class Site: Object {
    
    // Идентификатор сайта
    dynamic var id: Int = 0
    
    // Наименование сайта
    dynamic var name: String = ""
 
    override static func ignoredProperties() -> [String] {
        return ["id"]
    }
    
    convenience init(name: String) {
        self.init()
        self.name = name
    }
    
    func editName(name: String) {
        try! realmAS.write {
            self.name = name
        }
    }
    
    class func addSite(name: String) {
        
        let newSite = Site(name: name)
        
        try! realmAS.write {
            realmAS.add(newSite)
        }
    }
    
    class func getAllSites() -> [Site] {
        var result = [Site]()
        let search = realmAS.objects(Site)
        for i in 0..<search.count {
            result.append(search[i])
        }
        return result
    }
    
    class func getSiteBy(name: String) -> [Site] {
        var result = [Site]()
        let predicate = NSPredicate(format: "name CONTAINS [c]%@", name)
        let search = realmAS.objects(Site).filter(predicate)
        for i in 0..<search.count {
            result.append(search[i])
        }
        return result
    }
    
    class func deleteSite(site: Site) {
        try! realmAS.write {
            realmAS.delete(site)
        }
    }
}
