//
//  FirstViewController.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 29.09.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	let itunesAPI = ITunesAPI()
	
	var users: [User] {
		return User.getAllUsers()
	}
	
	//MARK: - service methods
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
		
		itunesAPI.fetchITunesData(.theKillers) { result in
			switch result {
			case .success(let iTunesData):
				print("The first track is \(iTunesData.track)")
			default:
				return
			}
		}
		
		anyStatAPI.fetchUsers { result in
			
			print("Test. Result is \(result)")
			
			switch result {
			case .success(let users):
				for user in users {
					let id = user.id
					dispatch_async(dispatch_get_main_queue(), {
						if User.getUserByID(id).count == 0 {
							User.addUser(user)
						}
						self.tableView.reloadData()
					})
					
				}
			case .error(let error):
				print(error)
			}
		}
	}
	
	override func viewWillAppear(animated: Bool) {
		navigationController?.topViewController?.title = "Users"
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == Segues.detailUser {
			let controller = segue.destinationViewController as! UserDetailViewController
			controller.delegate = self
			if let indexPath = tableView.indexPathForCell(sender as! UITableViewCell) {
					controller.userToDisplay = users[indexPath.row]
				
				}
			
		} else
		if segue.identifier == Segues.addUser {
			let navController = segue.destinationViewController as! UINavigationController
			let controller = navController.topViewController as! AddUserViewController
			controller.delegate = self
			print("\(controller.delegate)")
		}
	}
}

//MARK: - UITableView Delegate Methods
extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return users.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCellWithIdentifier(Cells.user, forIndexPath: indexPath)
		let i = indexPath.row
		cell.textLabel?.text = "\(users[i].nickname)"
		cell.detailTextLabel?.text = "\(users[i].name.uppercaseString) \(users[i].surname.uppercaseString)"
	
		return cell
	}
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		switch section {
		case 0:
			return "USERS"
		default:
			return ""
		}
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}
	
	func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
		cell.prepareDisclosureIndicator()
		cell.customize()
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 70
	}
}

//MARK: - Add User Delegate Methods
extension UsersViewController: AddUserViewControllerDelegate, UserDetailViewControllerDelegate {
	func addUserViewControllerDidCancel(controller: AddUserViewController) {
		dismissViewControllerAnimated(true, completion: nil)
	}
	
	func addUserViewController(controller: AddUserViewController, didFinishAdding user: User) {
		
		anyStatAPI.addUser(user) { result in
			
			print("test result adding users \(result)")
			
			switch result {
			case .success(let user):
				dispatch_async(dispatch_get_main_queue(), {
					User.addUser(user)
					self.tableView.reloadData()
				})
			case .error(let error):
				print("error is \(error)")
			}
		}
		
		dismissViewControllerAnimated(true, completion: nil)
		tableView.reloadData()
	}
	
	func userDetailViewController(controller: UserDetailViewController, didFinishDeleting user: User) {
		anyStatAPI.deleteUser(user)
		User.deleteUser(user)
		navigationController?.popToRootViewControllerAnimated(true)
		tableView.reloadData()
	}
}




