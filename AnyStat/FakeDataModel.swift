//
//  FakeDataModel.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 30.09.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import Foundation



class Users {
	static var users = ["Tom Hanks", "Felicity Jones", "Ben Foster"]
}
class Sources {
    static var sources = ["Lenta.ru", "RBC.ru", "bashorg.ru"]
}

class Persons {
    static var persons = ["Putin":["Путин", "Путина", "Путиным"], "Medvedev":["Медведев", "Медведева"], "Navalny":["Навальный", "Навального"]]
}
