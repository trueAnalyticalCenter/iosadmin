//
//  CatalogViewController.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 29.09.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import UIKit

class CatalogViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }

	override func viewWillAppear(animated: Bool) {
		navigationController?.topViewController?.title = "Catalog"
        tableView.reloadData()
	}

    @IBAction func add(sender: AnyObject) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let SourceAction = UIAlertAction(title: "Add source", style: .Default) { _ in
            self.performSegueWithIdentifier(Segues.goToNewSource, sender: nil)
        }
        alertController.addAction(SourceAction)
        
        
        let PersonAction = UIAlertAction(title: "Add person", style: .Default) { _ in
            self.performSegueWithIdentifier(Segues.goToNewPerson, sender: nil)
        }
        alertController.addAction(PersonAction)
		
        self.presentViewController(alertController, animated: true, completion: nil)
	}
}

extension CatalogViewController: UITableViewDelegate, UITableViewDataSource {
    enum Section: Int {
        case sources
        case persons
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Section.sources.rawValue:
            return Site.getAllSites().count
        case Section.persons.rawValue:
            return Person.getAllPersons().count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Cells.catalog, forIndexPath: indexPath)
        
        switch indexPath.section {
        case Section.sources.rawValue:
            cell.textLabel?.text = "\(Site.getAllSites()[indexPath.row].name)"
        case Section.persons.rawValue:
            cell.textLabel?.text = "\(Person.getAllPersons()[indexPath.row].name)"
        default:
            cell.textLabel?.text = ""
        }
       
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case Section.sources.rawValue:
            return "Sources"
        case Section.persons.rawValue:
            return "Names"
        default:
            return ""
        }
    }
	
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case Section.sources.rawValue:
            performSegueWithIdentifier(Segues.goToSource, sender: nil)
            
        case Section.persons.rawValue:
            performSegueWithIdentifier(Segues.goToPerson, sender: nil)
            
        default:
            return
        }
        
        tableView.deselectRowAtIndexPath(tableView.indexPathForSelectedRow!, animated: true)
    }
	
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.goToSource {
			let controller = segue.destinationViewController as! SourceTableViewController
			if let indexPath = tableView.indexPathForSelectedRow {
				controller.site = Site.getAllSites()[indexPath.row]
			}
        }
        
        if segue.identifier == Segues.goToPerson {
            let controller = segue.destinationViewController as! PersonTableViewController
			if let indexPath = self.tableView.indexPathForSelectedRow {
				controller.person = Person.getAllPersons()[indexPath.row]
			}
        }
    }
	func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
		cell.prepareDisclosureIndicator()
		cell.customize()
		
    }
}
