//
//  Keyword.swift
//  AnyStat
//
//  Created by Vitaly on 03.10.16.
//  Copyright © 2016 True Analytical Center. All rights reserved.
//

import Foundation
import RealmSwift

class Keyword: Object {
    
    // Идентификатор ключевого слова
    dynamic var id: Int = 0
    
    // Название ключевого слова
    dynamic var name: String = ""
    
    // Идентификатор личности, которой соответствует данное ключевое слово
    dynamic var PersonID: Int = 0
    
    override static func ignoredProperties() -> [String] {
        return ["id"]
    }
}
