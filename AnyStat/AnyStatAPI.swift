//
//  AnyStatAPI.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 20.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import Foundation

final class AnyStatAPI: APIClient {
	var configuration: NSURLSessionConfiguration
	lazy var session: NSURLSession = { return NSURLSession(configuration: self.configuration) }()
	
	init(config: NSURLSessionConfiguration) {
		self.configuration = config
	}
	
	convenience init() {
		self.init(config: NSURLSessionConfiguration.defaultSessionConfiguration())
	}
	
	func fetchUsers(completion: APIResult<[User]> -> Void) {
		let request = AnyStat.Users.request
		
		fetch(request, parse: { (data) -> [User]? in
			var users = [User]()
			guard let contents = data["content"] as? [JSON] else { return nil }
			
			for content in contents {
				if let user = User(content: content) {
					users.append(user)
				}
			}
			
			return users
			
			}, completion: completion)
	}
	
	func addUser(user: User, completion: APIResult<User> -> Void) {
		let request = AnyStat.AddUser(user.nickname, user.password).request
		request.HTTPMethod = "POST"
		
		fetch(request, parse: { (json) -> User? in
			if let user = User(content: json) {
				print("Test is there new user: \(user)")
				return user
			} else {
				return nil
			}
			}, completion: completion)
	}
	
	func deleteUser(user: User) {
		let request = AnyStat.DeleteUser(user.id).request
		request.HTTPMethod = "POST"
		let task = session.dataTaskWithRequest(request)
		task.resume()
	}
}

enum AnyStat: Endpoint {
	case Users
	case AddUser(String, String)
	case DeleteUser(Int)
	
	var baseURL: NSURL {
		return NSURL(string: "http://008080.guzc4mzxfy2dqlrsgmyq.nblz.ru/")!
	}
	
	var path: String {
		switch self {
		case .Users:
			return "users"
		case .AddUser(let name, let password):
			let path = String(format: "users/add?name=%@&pass=%@&admin_id=1", name, password)
			return path
		case .DeleteUser(let id):
			return "users/delete/\(id)"
		}
	}
	
	var request: NSMutableURLRequest {
		let url = NSURL(string: path, relativeToURL: baseURL)!
		print("url is \(url)")
		return NSMutableURLRequest(URL: url)
	}
	
}

