//
//  User.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 07.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
	dynamic var id: Int = 0
	dynamic var name: String = ""
	dynamic var surname: String = ""
	dynamic var nickname: String = ""
	dynamic var password = ""
	
//	override static func ignoredProperties() -> [String] {
//		return ["id"]
//	}
	
	convenience init(name: String, surname: String, nickname: String, password: String) {
		self.init()
		self.name = name
		self.surname = surname
		self.nickname = nickname
		self.password = password
	}
	
	convenience init?(content: JSON) {
		self.init()
	
		guard let name = content["username"] as? String else { return nil }
		guard let id = content["id"] as? Int else { return nil }
		guard let password = content["password"] as? String else { return nil }
		
		self.nickname = name
		self.id = id
		self.password = password
		
	}
	
	class func addUser(name: String, surname: String, nickname: String, password: String) {
		let newUser = User(name: name, surname: surname, nickname: nickname, password: password)
		try! realmAS.write {
			realmAS.add(newUser)
		}
	}
	
	class func addUser(user: User) {
		try! realmAS.write {
			realmAS.add(user)
		}
	}
	
	class func getAllUsers() -> [User] {
		var result = [User]()
		let search = realmAS.objects(User)
		for i in 0..<search.count {
			result.append(search[i])
		}
		return result
	}
	
	class func getUserBy(name: String) -> [User] {
		var result = [User]()
		let predicate = NSPredicate(format: "name CONTAINS [c]%@", name)
		let search = realmAS.objects(User).filter(predicate)
		for i in 0..<search.count {
			result.append(search[i])
		}
		return result
	}
	
	class func getUserByID(id: Int) -> [User] {
		var result = [User]()
		let predicate = NSPredicate(format: "id == %d", id)
		let search = realmAS.objects(User).filter(predicate)
		for i in 0..<search.count {
			result.append(search[i])
		}
		return result
	}

	
	class func deleteUser(user: User) {
		try! realmAS.write {
			realmAS.delete(user)
		}
	}
	
	class func deleteAllUsers() {
		let allUsers = getAllUsers()
		try! realmAS.write {
			allUsers.forEach { realmAS.delete($0) }
		}
	}
}
