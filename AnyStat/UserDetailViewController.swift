//
//  UserDetailViewController.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 03.10.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import UIKit

protocol UserDetailViewControllerDelegate: class {
	func userDetailViewController(controller: UserDetailViewController, didFinishDeleting user: User)
}

class UserDetailViewController: UIViewController {
	
	var userToDisplay: User?
	weak var delegate: UserDetailViewControllerDelegate?
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var surnameLabel: UILabel!
	@IBOutlet weak var nickNameLabel: UILabel!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

	override func viewWillAppear(animated: Bool) {
		if let user = userToDisplay {
			nameLabel.text = user.name.uppercaseString
			surnameLabel.text = user.surname.uppercaseString
			nickNameLabel.text = user.nickname
		}
	}
	
	@IBAction func deleteButton(sender: UIButton) {
		if let user = userToDisplay {
			delegate?.userDetailViewController(self, didFinishDeleting: user)
		}
	}
}
