//
//  Person.swift
//  AnyStat
//
//  Created by Vitaly on 03.10.16.
//  Copyright © 2016 True Analytical Center. All rights reserved.
//

import Foundation
import RealmSwift

class Person: Object {
    // Идентификатор личности
    dynamic var id: Int = 0
    // Наименование личности
    dynamic var name: String = ""
    //Ключевые слова личности
    let keywords = List<Keyword>()
    
    override static func ignoredProperties() -> [String] {
        return ["id"]
    }
    
    convenience init(name: String) {
        self.init()
        self.name = name
    }
    
    func editName(name: String) {
        try! realmAS.write {
            self.name = name
        }
    }
    func addKeywordWithName(keyword:String) {
        let newKeyword = Keyword()
        newKeyword.name = keyword;
        newKeyword.PersonID = self.id
        try! realmAS.write {
            self.keywords.append(newKeyword)
        }
    }
    func addKeyword(newKeyword:Keyword) {
        try! realmAS.write {
            self.keywords.append(newKeyword)
        }
    }
    
    func getKeywords() -> [Keyword] {
        var result = [Keyword]()
        for keyword in keywords {
            result.append(keyword)
        }
        return result
    }
    
    class func addPerson(name: String) -> Person {
        let newPerson = Person(name: name)
        try! realmAS.write {
            realmAS.add(newPerson)
        }
        return newPerson
    }
    
    class func getAllPersons() -> [Person] {
        var result = [Person]()
        let search = realmAS.objects(Person)
        for i in 0..<search.count {
            result.append(search[i])
        }
        return result
    }
    
    class func getPersonBy(name: String) -> [Person] {
        var result = [Person]()
        let predicate = NSPredicate(format: "name CONTAINS [c]%@", name)
        let search = realmAS.objects(Person).filter(predicate)
        for i in 0..<search.count {
            result.append(search[i])
        }
        return result
    }
    
    class func deletePerson(person: Person) {
        try! realmAS.write {
            realmAS.delete(person)
        }
    }
    
    
}
