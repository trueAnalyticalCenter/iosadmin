//
//  extensions.swift
//  AnyStat
//
//  Created by Сергей Гамаюнов on 29.09.16.
//  Copyright © 2016 Сергей Гамаюнов. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
	static let brand = UIColor(red: 228/255, green: 86/255, blue: 85/255, alpha: 1.0)
	static let dark = UIColor(red: 159/255, green: 50/255, blue: 47/255, alpha: 1.0)
	static let blackOpacity = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
}

extension UITableViewCell {
	func prepareDisclosureIndicator() {
		for case let button as UIButton in subviews {
			let image = button.backgroundImageForState(.Normal)?.imageWithRenderingMode(.AlwaysTemplate)
			button.setBackgroundImage(image, forState: .Normal)
		}
	}
	
	func customize() {
		textLabel?.textColor = Colors.dark
		detailTextLabel?.textColor = Colors.blackOpacity
	}
}

struct Segues {
	static let addUser = "AddUser"
	static let detailUser = "DetailUser"
	static let goToSource = "GoToSource"
	static let goToPerson = "GoToPerson"
	static let goToNewSource = "GoToNewSource"
	static let goToNewPerson = "GoToNewPerson"
}


struct Cells {
	static let catalog = "CatalogCell"
	static let user = "UserCell"
}
