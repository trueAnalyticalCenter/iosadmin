//
//  Page.swift
//  AnyStat
//
//  Created by Vitaly on 03.10.16.
//  Copyright © 2016 True Analytical Center. All rights reserved.
//

import Foundation
import RealmSwift

class Page: Object {
    
    // Идентификатор страницы сайта
    dynamic var id: Int = 0
    
    // Полный URL адрес страницы
    dynamic var url: String = ""
    
    // Идентификатор сайта (ресурса), который предоставлен  администратором для анализа
    dynamic var SiteID: Int = 0
    
    // Дата и время обнаружения страницы системой
    dynamic var FoundDateTime = NSDate()
    
    // Дата и время последней проверки на упоминания
    dynamic var LastScanDate = NSDate()
    
    override static func ignoredProperties() -> [String] {
        return ["id"]
    }
}
